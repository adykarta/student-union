from django.test import TestCase
from django.urls import resolve
from . import views

class UrlTests(TestCase):
    def test_homepage_url(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_invalid_url(self):
        response = self.client.get('/invalid-url/')
        self.assertEqual(response.status_code, 404)

class HomepageTests(TestCase):
    def test_homepage_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)
    
    def test_homepage_using_index_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'main/index.html')

    def test_homepage_content(self):
        response = self.client.get('/')
        self.assertContains(response, 'Hello, World!')
